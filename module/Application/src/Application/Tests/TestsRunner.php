<?php
/**
 * @author dnahrebecki
 * Date:   23.04.15
 */

namespace Application\Tests;

use Application\Tests\Cases\TestCaseInterface;

class TestsRunner
{
    /** @var TestCaseInterface[] */
    protected $testCases;

    public function run($testCaseName)
    {
        if (empty($this->testCases)) {
            throw new \LogicException('There are no test cases found');
        }

        if (!array_key_exists($testCaseName, $this->testCases)) {
            throw new \InvalidArgumentException(
                sprintf('Test case %s not found!', $testCaseName)
            );
        }

        $testCase = $this->testCases[$testCaseName];
        $testCase->run();
    }

    /**
     * @param TestCaseInterface $testCase
     *
     * @return TestsRunner
     */
    public function addTestCase(TestCaseInterface $testCase)
    {
        $this->testCases[$testCase->getName()] = $testCase;

        return $this;
    }
}