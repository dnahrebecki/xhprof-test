<?php
/**
 * @author dnahrebecki
 * Date:   23.04.15
 */

namespace Application\Tests;

use Application\Tests\Cases\CreateObject;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TestsRunnerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $testRunner = new TestsRunner();
        $testRunner
            ->addTestCase(new CreateObject());

        return $testRunner;
    }
}