<?php
/**
 * @author dnahrebecki
 * Date:   23.04.15
 */

namespace Application\Tests\Cases;

interface TestCaseInterface
{
    public function getName();

    public function run();
}