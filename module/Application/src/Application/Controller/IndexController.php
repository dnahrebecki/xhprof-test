<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Tests\TestsRunner;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $testCaseName = $this->params()->fromQuery('testCase');

        if (!empty($testCaseName)) {
            /** @var TestsRunner $testRunner */
            $testRunner = $this->getServiceLocator()->get('TestRunner');
            $testRunner->run($testCaseName);
        }

        return new ViewModel();
    }
}
